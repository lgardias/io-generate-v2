package entity;

import entity.util.EntityAvailabilityTime;

import javax.persistence.*;

/**
 * Defines when a {@link #room} assigned to this object
 * is accessible.
 *
 * @author Karol Domański
 */
@Entity
@Table(name = "room_availability_time")
public class RoomAvailabilityTime extends EntityAvailabilityTime
{
    private Room room;

    @Override
    public int hashCode()
    {
        return ((this.available) ? 1231 : 1237) * room.hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomAvailabilityTime that = (RoomAvailabilityTime) o;

        if (available != that.available) return false;

        return true;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "room_id", referencedColumnName = "id", nullable = false)
    public Room getRoom()
    {
        return room;
    }

    public void setRoom(Room room)
    {
        this.room = room;
    }
}
