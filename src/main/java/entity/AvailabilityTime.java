package entity;

import entity.util.AbstractModelObject;
import entity.util.EntityAvailabilityTime;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a time interval starting at {@link #start}
 * and ending at {@link #end} when a specific resource can
 * be either available or accessible.
 *
 * @author Karol Domański
 */
@Entity
@Table(name = "availability_time")
public class AvailabilityTime extends AbstractModelObject
{
    private int id;
    /** Starting time. */
    private Time start;
    /** Ending time. */
    private Time end;
    /** Day of the week this time interval is on. */
    private Weekday weekday;
    private List<LecturerAvailabilityTime> lecturerAvailabilityTimes = new ArrayList<LecturerAvailabilityTime>();
    private List<RoomAvailabilityTime> roomAvailabilityTimes = new ArrayList<RoomAvailabilityTime>();

    public AvailabilityTime(Time start, Time end, Weekday weekday)
    {
        this.start = start;
        this.end = end;
        this.weekday = weekday;
    }

    public AvailabilityTime() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "start")
    public Time getStart()
    {
        return start;
    }

    public void setStart(Time start)
    {
        changeSupport.firePropertyChange("start", this.start, start);
        this.start = start;
    }

    @Basic
    @Column(name = "end")
    public Time getEnd()
    {
        return end;
    }

    public void setEnd(Time end)
    {
        changeSupport.firePropertyChange("end", this.end, end);
        this.end = end;
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result += 31 * result + (start != null ? start.hashCode() : 0);
        result += 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AvailabilityTime that = (AvailabilityTime) o;

        if (id != that.id) return false;
        if (start != null ? !start.equals(that.start) : that.start != null) return false;
        if (end != null ? !end.equals(that.end) : that.end != null) return false;

        return true;
    }

    @ManyToOne
    @JoinColumn(name = "weekday_id", referencedColumnName = "id", nullable = false)
    public Weekday getWeekday()
    {
        return weekday;
    }

    public void setWeekday(Weekday weekday)
    {
        changeSupport.firePropertyChange("weekday", this.weekday, weekday);
        this.weekday = weekday;
    }

    @OneToMany(mappedBy = "availabilityTime", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<LecturerAvailabilityTime> getLecturerAvailabilityTimes()
    {
        return lecturerAvailabilityTimes;
    }

    public void setLecturerAvailabilityTimes(List<LecturerAvailabilityTime> lecturerAvailabilityTimes)
    {
        this.lecturerAvailabilityTimes = lecturerAvailabilityTimes;
    }

    @OneToMany(mappedBy = "availabilityTime", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<RoomAvailabilityTime> getRoomAvailabilityTimes()
    {
        return roomAvailabilityTimes;
    }

    public void setRoomAvailabilityTimes(List<RoomAvailabilityTime> roomAvailabilityTimes)
    {
        this.roomAvailabilityTimes = roomAvailabilityTimes;
    }

    /**
     * Tests whether a specified list contains at least one {@link AvailabilityTime availability
     * time} interval conflicting with this availability time.
     * <p>For example, a time interval <i>08:00-09:30</i> has nothing
     * to do with <i>09:45-11:15</i>. On the other hand, it conflicts
     * with <i>08:30-09:20</i>.</p>
     *
     * @param entityAvailabilityTimes   the list whose elements are to be checked
     * @return                          {@code true} if at least one of the elements in the list
     *                                  conflicts with this object; {@code false} otherwise
     */
    public boolean isConflicting(List<? extends EntityAvailabilityTime> entityAvailabilityTimes)
    {
        for (EntityAvailabilityTime eat : entityAvailabilityTimes)
        {
            final AvailabilityTime at = eat.getAvailabilityTime();

            // skip if these are entirely different weekdays
            if (at.getWeekday() != weekday)
                continue;

            final Time otherStart = at.getStart();
            final Time otherEnd = at.getEnd();

             /*
                Three possible scenarios that cause a conflict:
                a) 08:00-09:30 & 08:10-XX:XX
                b) 08:00-09:30 & XX:XX-08:15
                c) 08:00-09:30 & 08:15-09:00
             */
            if ((
                    start.compareTo(otherStart) <= 0 && end.compareTo(otherStart) >= 0) ||
                    (start.compareTo(otherEnd) <= 0 && end.compareTo(otherEnd) >= 0) ||
                    (start.compareTo(otherStart) >= 0 && end.compareTo(otherEnd) <= 0))
                return true;
        }

        return false;
    }
}
