package entity.util;

import org.jdesktop.observablecollections.ObservableList;
import org.jdesktop.observablecollections.ObservableListListener;

import javax.persistence.EntityManager;
import javax.persistence.TransactionRequiredException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.UnaryOperator;

/**
 * Custom {@link ArrayList} which is self-aware of all the entities that are yet to be
 * persisted or removed from the database.
 *
 * <p>It manages the lists {@link #persistQueue} and {@link #removeQueue} automatically whenever
 * an object is added or removed from this list, so one does not have to be concerned about it.</p>
 *
 * <p>Since this is an {@link ObservableList ObservableList}, it notifies all of its {@link #listeners}
 * of any changes applied to any of the elements' properties. It makes it fully compatible with JavaBeans
 * JTable binding.</p>
 *
 * @author Karol Domański
 *
 * @param <E> the type of elements in this list
 */
public class EntityList<E> extends ArrayList<E> implements ObservableList<E>
{
    /**
     * Holds all the entities that are meant to be removed
     * from the database.
     */
    private final ArrayList<E> removeQueue = new ArrayList<E>();

    /**
     * Holds all the new entities that are meant to be persisted
     * to the database.
     */
    private final ArrayList<E> persistQueue = new ArrayList<E>();

    /**
     * Whether this list supports changes of its elements' properties.
     */
    private final boolean supportsElementPropertyChanged;

    /**
     * Holds all listeners that are notified when the list or any of its
     * elements' properties change.
     */
    private List<ObservableListListener> listeners = new CopyOnWriteArrayList<>();

    /**
     * Constructs an empty list with the specified initial capacity. By default, the list
     * will support changes of elements' properties.
     *
     * @param  initialCapacity  the initial capacity of the list
     * @throws IllegalArgumentException if the specified initial capacity
     *         is negative
     */
    public EntityList(int initialCapacity)
    {
        super(initialCapacity);
        supportsElementPropertyChanged = true;
    }

    /**
     * Constructs an empty list with an initial capacity of ten. Depending on the argument,
     * the list may or may not support changes of elements' properties.
     *
     * @param supportsElementPropertyChanged    whether this list should support changes
     *                                          of elements' properties
     */
    public EntityList(boolean supportsElementPropertyChanged)
    {
        super();
        this.supportsElementPropertyChanged = supportsElementPropertyChanged;
    }

    /**
     * Constructs an empty list with the specified initial capacity. Depending
     * on the second argument, the list may or may not support changes of
     * elements' properties.
     *
     * @param  initialCapacity  the initial capacity of the list
     * @param supportsElementPropertyChanged    whether this list should support changes
     *                                          of elements' properties
     * @throws IllegalArgumentException if the specified initial capacity
     *         is negative
     */
    public EntityList(int initialCapacity, boolean supportsElementPropertyChanged)
    {
        super(initialCapacity);
        this.supportsElementPropertyChanged = supportsElementPropertyChanged;
    }

    /**
     * Constructs an empty list with an initial capacity of ten. By default, the list
     * will support changes of elements' properties.
     */
    public EntityList()
    {
        super();
        supportsElementPropertyChanged = true;
    }

    @Override
    public boolean add(E e)
    {
        for (ObservableListListener listener : listeners)
            listener.listElementsAdded(this, size() + 1, 1);

        persistQueue.add(e);
        removeQueue.remove(e);

        return super.add(e);
    }

    @Override
    public void add(int index, E element)
    {
        for (ObservableListListener listener : listeners)
            listener.listElementsAdded(this, index, 1);

        persistQueue.add(element);
        removeQueue.remove(element);

        super.add(index, element);
    }

    /**
     * {@inheritDoc}
     *
     * <p><strong>Be aware</strong> that adding elements using this
     * method does not enqueue them in {@link #persistQueue}! It is advised
     * to use this method only when objects of the specified collection
     * are already persistent in the database.</p>
     */
    @Override
    public boolean addAll(Collection<? extends E> c)
    {
        final int index = size() + 1;
        final boolean hasChanged = super.addAll(c);

        if(hasChanged)
            for (ObservableListListener listener : listeners)
                listener.listElementsAdded(this, index, c.size());

        return hasChanged;
    }

    /**
     * {@inheritDoc}
     *
     * <p><strong>Be aware</strong> that adding elements using this
     * method does not enqueue them in {@link #persistQueue}! It is advised
     * to use this method only when objects of the specified collection
     * are already persistent in the database.</p>
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c)
    {
        final boolean hasChanged = super.addAll(index, c);

        if(hasChanged)
            for (ObservableListListener listener : listeners)
                listener.listElementsAdded(this, index, c.size());

        return hasChanged;
    }

    @Override
    public E remove(int index)
    {
        E element = super.remove(index);

        for (ObservableListListener listener : listeners)
            listener.listElementsRemoved(this, index, java.util.Collections.singletonList(element));

        removeQueue.add(element);
        persistQueue.remove(element);

        return element;
    }

    @Override
    public boolean remove(Object o)
    {
        final int index = indexOf(o);

        if(index > -1)
        {
            remove(index);
            return true;
        }

        return false;
    }

    @Override
    public E set(int index, E element)
    {
        E oldElement = super.set(index, element);

        for (ObservableListListener listener : listeners)
            listener.listElementReplaced(this, index, oldElement);

        persistQueue.add(element);
        persistQueue.remove(oldElement);
        removeQueue.add(oldElement);
        removeQueue.remove(element);

        return oldElement;
    }

    /**
     * {@inheritDoc}
     *
     * <p>Naturally, this method also {@link #clearQueues() clears} all the queues.</p>
     */
    @Override
    public void clear()
    {
        List<E> dup = new ArrayList<E>(this);
        super.clear();
        clearQueues();

        if (dup.size() != 0)
            for (ObservableListListener listener : listeners)
                listener.listElementsRemoved(this, 0, dup);
    }

    /**
     * Persists to the database all the entities held in {@link #persistQueue},
     * removes from the database all the entities from {@link #removeQueue}
     * and then {@link #clearQueues() clears all the queues}.
     *
     * @param entityManager     open EntityManager
     * @throws IllegalStateException            when entityManager is either {@code null} or closed
     * @throws TransactionRequiredException     when a transaction is required but is not active
     */
    public void persistOrRemove(EntityManager entityManager) throws IllegalStateException, TransactionRequiredException
    {
        if(entityManager == null || !entityManager.isOpen())
            throw new IllegalStateException("EntityManager is either null or closed.");

        for (Object o : removeQueue)
            entityManager.remove(o);

        for (Object o : persistQueue)
            entityManager.persist(o);

        clearQueues();
    }

    /**
     * Clears {@link #removeQueue} and {@link #persistQueue} array lists.
     */
    private void clearQueues()
    {
        persistQueue.clear();
        removeQueue.clear();
    }

    /**
     * Invoked when the specific element of this list had any of its
     * properties changed.
     *
     * @param index     the index of element that changed
     */
    private void fireElementChanged(int index)
    {
        for (ObservableListListener listener : listeners)
            listener.listElementPropertyChanged(this, index);
    }

    @Override
    public void addObservableListListener(ObservableListListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void removeObservableListListener(ObservableListListener listener)
    {
        listeners.remove(listener);
    }

    @Override
    public boolean supportsElementPropertyChanged()
    {
        return supportsElementPropertyChanged;
    }

    /**
     * @deprecated  Because this method isn't safe for {@link ObservableList}.
     */
    @Override
    @Deprecated
    protected void removeRange(int fromIndex, int toIndex)
    {
        super.removeRange(fromIndex, toIndex);
    }

    /**
     * @deprecated  Because this method isn't safe for {@link ObservableList}.
     */
    @Override
    @Deprecated
    public boolean removeAll(Collection<?> c)
    {
        return super.removeAll(c);
    }

    /**
     * @deprecated  Because this method isn't safe for {@link ObservableList}.
     */
    @Override
    @Deprecated
    public void replaceAll(UnaryOperator<E> operator)
    {
        super.replaceAll(operator);
    }
}
