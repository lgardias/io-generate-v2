package entity.util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Abstract base class for model objects with support for bound properties.
 * Bound properties fire property change events when changed.
 * The Beans Binding library invokes {@link #addPropertyChangeListener} and
 * {@link #removePropertyChangeListener} and listens to property changes.
 *
 * @author Karol Domański
 */
public abstract class AbstractModelObject
{
    protected final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        changeSupport.removePropertyChangeListener(listener);
    }
}
