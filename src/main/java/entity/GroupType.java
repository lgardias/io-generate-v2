package entity;

import entity.util.EntityType;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a type of a group, whether it is a whole "year"
 * (in terms of group, not time), laboratory group or student
 * research group.
 *
 * @author Karol Domański
 * @author Grzegorz Chromik
 */
@Entity
@Table(name = "group_type")
public class GroupType extends EntityType
{
    /** Holds all the {@link Group groups} this entity is assigned to. */
    private List<Group> groups = new ArrayList<Group>();

    public GroupType(String name)
    {
        this.name = name;
    }

    public GroupType()
    {}

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupType groupType = (GroupType) o;

        if (id != groupType.id) return false;
        if (name != null ? !name.equals(groupType.name) : groupType.name != null) return false;

        return true;
    }

    @OneToMany(mappedBy = "groupType", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    public List<Group> getRooms()
    {
        return groups;
    }

    public void setRooms(List<Group> groups)
    {
        changeSupport.firePropertyChange("groups", this.groups, groups);
        this.groups = groups;
    }
}
