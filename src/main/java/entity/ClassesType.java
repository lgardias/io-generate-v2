package entity;

import entity.util.EntityType;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a type of classes (ex: laboratory, project, lecture, etc.).
 *
 * @author Karol Domański
 * @author Grzegorz Chromik
 */
@Entity
@Table(name = "classes_type")
public class ClassesType extends EntityType
{
    /** Holds all the {@link Classes classes} this entity is assigned to. */
    private List<Classes> classesList = new ArrayList<Classes>();

    public ClassesType(String name)
    {
        this.name = name;
    }

    public ClassesType()
    {}

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassesType roomType = (ClassesType) o;

        if (id != roomType.id) return false;
        if (name != null ? !name.equals(roomType.name) : roomType.name != null) return false;

        return true;
    }

    @OneToMany(mappedBy = "classesType", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Classes> getClassesList()
    {
        return classesList;
    }

    public void setClassesList(List<Classes> classesList)
    {
        changeSupport.firePropertyChange("classesList", this.classesList, classesList);
        this.classesList = classesList;
    }
}