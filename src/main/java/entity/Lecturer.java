package entity;

import entity.util.AbstractModelObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a lecturer, tutor, teacher or whoever.
 *
 * @author Karol Domański
 * @author Grzegorz Chromik
 */
@Entity
@Table(name = "lecturer")
public class Lecturer extends AbstractModelObject
{
    private int id;
    /** Title or degree of this person. */
    private String degree;
    private String firstName;
    private String lastName;
    private List<LecturerAvailabilityTime> lecturerAvailabilityTimes = new ArrayList<LecturerAvailabilityTime>();
    private List<PhysicalClasses> physicalClasses = new ArrayList<>();

    public Lecturer(String degree, String firstName, String lastName)
    {
        this.degree = degree;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Lecturer()
    {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "degree")
    public String getDegree()
    {
        return degree;
    }

    public void setDegree(String degree)
    {
        changeSupport.firePropertyChange("degree", this.degree, degree);
        this.degree = degree;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        changeSupport.firePropertyChange("firstName", this.firstName, firstName);
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        changeSupport.firePropertyChange("lastName", this.lastName, lastName);
        this.lastName = lastName;
    }

    @Override
    public int hashCode()
    {
        int result = id;
        result += 31 * result + (degree != null ? degree.hashCode() : 0);
        result += 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result += 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lecturer lecturer = (Lecturer) o;

        if (id != lecturer.id) return false;
        if (degree != null ? !degree.equals(lecturer.degree) : lecturer.degree != null) return false;
        if (firstName != null ? !firstName.equals(lecturer.firstName) : lecturer.firstName != null) return false;
        if (lastName != null ? !lastName.equals(lecturer.lastName) : lecturer.lastName != null) return false;

        return true;
    }

    @OneToMany(mappedBy = "lecturer", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<LecturerAvailabilityTime> getLecturerAvailabilityTimes()
    {
        return lecturerAvailabilityTimes;
    }

    public void setLecturerAvailabilityTimes(List<LecturerAvailabilityTime> lecturerAvailabilityTimes)
    {
        changeSupport.firePropertyChange("lecturerAvailabilityTimes", this.lecturerAvailabilityTimes, lecturerAvailabilityTimes);
        this.lecturerAvailabilityTimes = lecturerAvailabilityTimes;
    }

    @OneToMany(mappedBy = "lecturer", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<PhysicalClasses> getPhysicalClasses()
    {
        return physicalClasses;
    }

    public void setPhysicalClasses(List<PhysicalClasses> physicalClasses)
    {
        changeSupport.firePropertyChange("physicalClassesList", this.physicalClasses, physicalClasses);
        this.physicalClasses = physicalClasses;
    }
}
