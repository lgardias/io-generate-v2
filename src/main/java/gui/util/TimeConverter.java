package gui.util;

import org.jdesktop.beansbinding.Converter;

import javax.swing.*;
import java.sql.Time;

/**
 * Time converter used by JavaBean to convert {@link Time sql.Time}
 * format to {@link String} during data binding process.
 *
 * <p>Even though it's very straightforward, it cannot be done
 * automatically.</p>
 *
 * @author Karol Domański
 */
public class TimeConverter extends Converter<Time, String>
{
    @Override
    public String convertForward(Time value)
    {
        return value.toString();
    }

    @Override
    public Time convertReverse(String value)
    {
        return Time.valueOf(value);
    }

    /**
     * Constructs a {@link Time sql.Time} object from values
     * inserted by user into two separate {@link JSpinner JSpinners}.
     *
     * @param hourJSpinner   JSpinner holding an hour of a time
     * @param minuteJSpinner JSpinner holding a minute of a time
     * @return  appropriate {@code sql.Time} object if construction was
     *          successful; {@code null} otherwise
     */
    public Time buildTime(JSpinner hourJSpinner, JSpinner minuteJSpinner)
    {
        if(hourJSpinner == null || minuteJSpinner == null)
            throw new IllegalArgumentException("Neither of the specified arguments is allowed to be null.");

        final StringBuilder builder = new StringBuilder(8);

        // read an hour
        builder.append(hourJSpinner.getValue().toString());

        // if an hour is X, it needs to be transformed into 0X
        if(builder.length() == 1)
            builder.insert(0, '0');

        // append `:` symbol
        builder.append(':');

        // read a minute
        builder.append(minuteJSpinner.getValue().toString());

        // if a minute is X, it needs to be transformed into 0X
        if(builder.length() == 4)
            builder.insert(3, '0');

        // append the remaining seconds
        builder.append(":00");

        // try to build a time
        Time time;

        try
        {
            time = Time.valueOf(builder.toString());
        }
        catch (IllegalArgumentException e)
        {
            time = null;
        }

        return time;
    }
}
