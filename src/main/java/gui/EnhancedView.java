/*
 * Created by JFormDesigner on Mon Dec 12 21:22:59 CET 2016
 */

package gui;

import entity.*;
import entity.util.EntityList;
import generate.Generate;
import generate.Input;
import generate.Output;
import gui.util.*;
import gui.util.MessageDialog;
import gui.util.TimeConverter;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.beansbinding.*;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import singletons.DataManager;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Main frame of the program. Provides user with visual
 * interface for data edit or removal. Acts as View in MVC
 * pattern with as little Controller as possible.
 *
 * @author Karol Domański
 * @author Przemysław Dębski
 */
public class EnhancedView extends JFrame
{
    private DataManager dataManager = DataManager.getInstance();
    private LoaderDialog loaderDialog;

    public EnhancedView()
    {
        initComponents();

        // initialize LoaderDialog with this JFrame
        loaderDialog = new LoaderDialog(this);

        // load entities from the database
		loaderDialog.beginLoading();
    }

    private void thisWindowClosing(WindowEvent e)
    {
		try
		{
			if(!dataManager.saveOrUpdateEntities())
				MessageDialog.showUnsuccessfulDataSaveDialog(this);
		}
		catch (IllegalStateException ignored)
		{
			// just let the program close
		}
	}

    private void saveLecturerButtonActionPerformed()
    {
    	Generate ss = new Generate(dataManager);
		ss.generateScheduleSimply();
		for(int i=1;i<3;i++)
		ss.showSchedule(i);
		Output find=new Output(ss.schedule);
		find.findGroupPlan(1);
/*
		System.out.println(ss.DaneWejsciowe.listClasses[2].getType()+"\t");
		for(int i = 0; i<8;i++){
			for (int j = 0; j< 6; j++){
				System.out.print(ss.DaneWejsciowe.roomTab[2].availableTab[j][i]+"\t");
			}
			System.out.println("");
		}
/*

        final String degree = lecturerDegreeTextField.getText();
        final String firstName = lecturerFstNameTextField.getText();
        final String lastName = lecturerSndNameTextField.getText();

        if (degree.length() == 0 || firstName.length() == 0 || lastName.length() == 0)
        {
            MessageDialog.showInvalidInputDialog(this);
            return;
        }

        // detailed input validation
        final StringValidator validator = new StringValidator();

        Validator.Result result = validator.validate(degree);
        if(result == null) result = validator.validate(firstName);
        if(result == null) result = validator.validate(lastName);
		if(result != null)
		{
			MessageDialog.showInvalidInputDialog(this, result.getDescription());
			return;
		}

		// save the entity
        lecturers.add(new Lecturer(degree, firstName, lastName));

        // clear text fields
        lecturerDegreeTextField.setText(null);
        lecturerFstNameTextField.setText(null);
        lecturerSndNameTextField.setText(null);
*/
    }

    private void deleteSelectedLecturersButtonActionPerformed()
    {
        if (selectedLecturer == null)
            return;

        if (MessageDialog.showAreYouSureDialog(this) != 0)
            return;

        lecturers.remove(selectedLecturer);
        selectedLecturer = null;
		lecturerAvailabilityTimesTable.setModel(new DefaultTableModel());
    }

    private void createUIComponents()
    {
        // TODO: add custom component creation code here
    }

    private void lecturersTableMouseClicked()
    {
        int index = lecturersTable.getSelectedRow();

        if (index > -1)
		{
			selectedLecturer = lecturers.get(lecturersTable.getSelectedRow());
			rebindLecturerAvailabilityTimes();
		}
        else
		{
			selectedLecturer = null;
			lecturerAvailabilityTimesTable.setModel(new DefaultTableModel());
		}
    }

    // TODO it seems this method and addRoomAvailabilityTimesButtonActionPerformed() are almost identical
    private void addLecturerAvailabilityTimesButtonActionPerformed()
    {
        if (selectedLecturer == null)
        {
            JOptionPane.showMessageDialog(
                    this,
                    "Proszę wybrać prowadzącego z listy obok.",
                    "Wybierz prowadzącego",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

		final int[] rows = availabilityTimesTable.getSelectedRows();
		final List<LecturerAvailabilityTime> lecturerAvailabilityTimes = selectedLecturer.getLecturerAvailabilityTimes();
		boolean conflictsDetected = false;

		for (int row : rows)
		{
			final AvailabilityTime at = dataManager.availabilityTimes.get(row);

			if(at.isConflicting(lecturerAvailabilityTimes))
			{
				conflictsDetected = true;
				continue;
			}

			final LecturerAvailabilityTime lat = new LecturerAvailabilityTime();
			lat.setLecturer(selectedLecturer);
			lat.setAvailabilityTime(at);
			lecturerAvailabilityTimes.add(lat);
		}

		if(conflictsDetected)
			MessageDialog.showConflictingTimesDialog(this);

        rebindLecturerAvailabilityTimes();
    }

    // TODO find a better way to refresh table's data!
    @SuppressWarnings("unchecked")
    private void rebindLecturerAvailabilityTimes()
    {
        BindingGroup lecturerAvailabilityTimesBindingGroup = new BindingGroup();
        TimeConverter timeConverter = new TimeConverter();

        JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
                selectedLecturer, (BeanProperty) BeanProperty.create("lecturerAvailabilityTimes"), lecturerAvailabilityTimesTable, "selectedLecturer");
        JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.start"))
                .setColumnName("Od")
                .setColumnClass(String.class)
                .setEditable(false);
        columnBinding.setConverter(timeConverter);
        columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.end"))
                .setColumnName("Do")
                .setColumnClass(String.class)
                .setEditable(false);
        columnBinding.setConverter(timeConverter);
        binding.addColumnBinding(BeanProperty.create("availabilityTime.weekday.name"))
                .setColumnName("Dzie\u0144")
                .setColumnClass(String.class)
                .setEditable(false);
        binding.addColumnBinding(BeanProperty.create("available"))
                .setColumnName("Dost\u0119pny?")
                .setColumnClass(Boolean.class);
        lecturerAvailabilityTimesBindingGroup.addBinding(binding);
        binding.bind();
        lecturerAvailabilityTimesBindingGroup.bind();
    }

    // TODO find a better way to refresh table's data!
    @SuppressWarnings("unchecked")
    private void rebindRoomAvailabilityTimes()
    {
        BindingGroup roomAvailabilityTimesBindingGroup = new BindingGroup();
        TimeConverter timeConverter = new TimeConverter();
        JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
                selectedRoom, (BeanProperty) BeanProperty.create("roomAvailabilityTimes"), roomAvailabilityTimesTable);
        JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.start"))
                .setColumnName("Od")
                .setColumnClass(String.class)
                .setEditable(false);
        columnBinding.setConverter(timeConverter);
        columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.end"))
                .setColumnName("Do")
                .setColumnClass(String.class)
                .setEditable(false);
        columnBinding.setConverter(timeConverter);
        binding.addColumnBinding(BeanProperty.create("availabilityTime.weekday.name"))
                .setColumnName("Dzie\u0144")
                .setColumnClass(String.class)
                .setEditable(false);
        binding.addColumnBinding(BeanProperty.create("available"))
                .setColumnName("Wolny?")
                .setColumnClass(Boolean.class);
        roomAvailabilityTimesBindingGroup.addBinding(binding);
        roomAvailabilityTimesBindingGroup.bind();
    }

    private void removeLecturerAvailabilityTimesButtonActionPerformed()
    {
        if (selectedLecturer == null)
            return;

        int[] rows = lecturerAvailabilityTimesTable.getSelectedRows();
        java.util.List<LecturerAvailabilityTime> lecturerAvailabilityTimes = selectedLecturer.getLecturerAvailabilityTimes();

        for (int i = 0, n = rows.length; i < n; i++)
        {
            // need to compensate for array shift to the left after an object is deleted
            lecturerAvailabilityTimes.remove(rows[i] - i);
        }

        rebindLecturerAvailabilityTimes();
    }

    private void saveClassesButtonActionPerformed()
    {
        final String name = classesNameTextField.getText();
        final ClassesType classesType = (ClassesType) classesTypeComboBox.getSelectedItem();

        if (name.length() == 0 || classesType == null)
        {
            MessageDialog.showInvalidInputDialog(this);
            return;
        }

		// save the entity
        final Classes classes = new Classes(name);
        classes.setClassesType(classesType);
        classesList.add(classes);

        // clear textfield
        classesNameTextField.setText(null);
    }

    private void deleteSelectedClassesButtonActionPerformed()
    {
        if (classesPoolTable.getSelectedRowCount() == 0)
            return;

        if (MessageDialog.showAreYouSureDialog(this) != 0)
            return;

        classesList.remove(classesPoolTable.getSelectedRow());
    }

    private void saveGroupButtonActionPerformed()
    {
        final String name = groupNameTextField.getText();
        final GroupType groupType = (GroupType) groupTypeComboBox.getSelectedItem();

        if (name.length() == 0 || groupType == null)
        {
            MessageDialog.showInvalidInputDialog(this);
            return;
        }

		// detailed input validation
		final StringValidator validator = new StringValidator();

		Validator.Result result = validator.validate(name);
		if(result != null)
		{
			MessageDialog.showInvalidInputDialog(this, result.getDescription());
			return;
		}

		// save the entity
        final Integer size = (Integer) groupSizeSpinner.getValue();
        final Group group = new Group(name, size);
        group.setGroupType(groupType);
        groups.add(group);

        // clear text fields
        groupNameTextField.setText(null);
    }

    private void deleteSelectedGroupsButtonActionPerformed()
    {
        if (groupsTable.getSelectedRowCount() == 0)
            return;

        if (MessageDialog.showAreYouSureDialog(this) != 0)
            return;

        groups.remove(groupsTable.getSelectedRow());
    }

    private void saveRoomButtonActionPerformed()
    {
        final String name = roomNameTextField.getText();
        final RoomType roomType = (RoomType) roomTypeComboBox.getSelectedItem();

        if (name.length() == 0 || roomType == null)
        {
            MessageDialog.showInvalidInputDialog(this);
            return;
        }

        final int capacity = (int) roomCapacitySpinner.getValue();
        final Room room = new Room(name, capacity, roomType);
        rooms.add(room);

        // clear text fields
        roomNameTextField.setText(null);
    }

    private void deleteSelectedRoomsButtonActionPerformed()
    {
		if (selectedRoom == null)
			return;

		if (MessageDialog.showAreYouSureDialog(this) != 0)
			return;

		rooms.remove(selectedRoom);
		selectedRoom = null;
		roomAvailabilityTimesTable.setModel(new DefaultTableModel());
    }

    private void removeRoomAvailabilityTimesButtonActionPerformed()
    {
        if (selectedRoom == null)
            return;

        int[] rows = roomAvailabilityTimesTable.getSelectedRows();
        java.util.List<RoomAvailabilityTime> roomAvailabilityTimes = selectedRoom.getRoomAvailabilityTimes();

        for (int i = 0, n = rows.length; i < n; i++)
        {
            // need to compensate for array shift to the left after an object is deleted
            roomAvailabilityTimes.remove(rows[i] - i);
        }

        rebindRoomAvailabilityTimes();
    }

    private void addRoomAvailabilityTimesButtonActionPerformed()
    {
        if (selectedRoom == null)
        {
            JOptionPane.showMessageDialog(
                    this,
                    "Proszę wybrać salę z listy obok.",
                    "Wybierz salę",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        final int[] rows = availabilityTimesTable.getSelectedRows();
        final List<RoomAvailabilityTime> roomAvailabilityTimes = selectedRoom.getRoomAvailabilityTimes();
        boolean conflictsDetected = false;

        for (int row : rows)
        {
        	final AvailabilityTime at = dataManager.availabilityTimes.get(row);

        	if(at.isConflicting(roomAvailabilityTimes))
			{
				conflictsDetected = true;
				continue;
			}

            final RoomAvailabilityTime rat = new RoomAvailabilityTime();
            rat.setRoom(selectedRoom);
            rat.setAvailabilityTime(at);
            roomAvailabilityTimes.add(rat);
        }

        if(conflictsDetected)
        	MessageDialog.showConflictingTimesDialog(this);

        rebindRoomAvailabilityTimes();
    }

	private void roomsTableMouseClicked()
    {
        final int index = roomsTable.getSelectedRow();

		if (index > -1)
		{
			selectedRoom = rooms.get(roomsTable.getSelectedRow());
			rebindRoomAvailabilityTimes();
		}
		else
		{
			selectedRoom = null;
			roomAvailabilityTimesTable.setModel(new DefaultTableModel());
		}
	}

	private void savePhysicalClassesButtonActionPerformed()
    {
		final Lecturer lecturer = (Lecturer) classesLecturersList.getSelectedValue();
		final Classes classes = (Classes) classesClassesList.getSelectedValue();
		final Group group = (Group) classesGroupsList.getSelectedValue();

		if(lecturer == null || classes == null || group == null)
        {
            MessageDialog.showInvalidInputDialog(this);
            return;
        }

        final PhysicalClasses physicalClasses = new PhysicalClasses();
		physicalClasses.setLecturer(lecturer);
		physicalClasses.setClasses(classes);
		physicalClasses.setGroup(group);
		physicalClassesList.add(physicalClasses);
	}

	private void deletePhysicalClassesButtonActionPerformed()
    {
		if (physicalClassesTable.getSelectedRowCount() == 0)
			return;

		if (MessageDialog.showAreYouSureDialog(this) != 0)
			return;

		physicalClassesList.remove(physicalClassesTable.getSelectedRow());
	}

	private void persistMenuItemActionPerformed()
	{
		try
		{
			if(!dataManager.saveOrUpdateEntities())
                MessageDialog.showUnsuccessfulDataSaveDialog(this);
		}
		catch (IllegalStateException e)
		{
			MessageDialog.showUnsuccessfulDataSaveDialog(this);
		}
	}

	private void loadMenuItemActionPerformed()
	{
		loaderDialog.beginLoading();
	}

	@SuppressWarnings("unchecked")
	private void addAvailabilityTimeButtonActionPerformed()
	{
		final ArrayList<Weekday> selectedWeekdays = new ArrayList<>(weekdaysList.getSelectedValuesList());

		if(selectedWeekdays.isEmpty())
		{
			MessageDialog.showInvalidInputDialog(this);
			return;
		}

		// build start and end times
		final TimeConverter converter = new TimeConverter();
		final Time start = converter.buildTime(availabilityTimeStartHourSpinner, availabilityTimeStartMinuteSpinner);
		final Time end = converter.buildTime(availabilityTimeEndHourSpinner, availabilityTimeEndMinuteSpinner);

		if(start == null || end == null)
		{
			MessageDialog.showInvalidInputDialog(this);
			return;
		}

		for (Weekday selectedWeekday : selectedWeekdays)
			availabilityTimes.add(new AvailabilityTime(start, end, selectedWeekday));
	}

	private void deleteAvailabilityTimesButtonActionPerformed()
	{
		final int[] rows = availabilityTimesTable.getSelectedRows();

		if(rows.length == 0)
		{
			JOptionPane.showMessageDialog(
					this,
					"Proszę wybrać godziny dostępności do usunięcia.",
					"Wybierz godziny",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if(MessageDialog.showAreYouSureDialog(this) != 0)
			return;

		for (int i = 0, n = rows.length; i < n; i++)
		{
			// need to compensate for array shift to the left after an object is deleted
			availabilityTimes.remove(rows[i] - i);
		}
	}

	private void classesPoolTableMouseClicked()
	{
		// TODO add your code here
	}

    private void initComponents()
    {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Nicholas Wilde
		JTabbedPane tabbedPane = new JTabbedPane();
		JPanel lecturersPanel = new JPanel();
		JPanel lecturerEditAndViewPanel = new JPanel();
		JPanel saveLecturerPanel = new JPanel();
		JLabel lecturerDegreeLabel = new JLabel();
		lecturerDegreeTextField = new JTextField();
		JLabel lecturerFstNameLabel = new JLabel();
		lecturerFstNameTextField = new JTextField();
		JLabel lecturerSndNameLabel = new JLabel();
		lecturerSndNameTextField = new JTextField();
		JButton saveLecturerButton = new JButton();
		JScrollPane lecturersScrollPane = new JScrollPane();
		lecturersTable = new JTable();
		JPanel lecturerOptionsPanel = new JPanel();
		JButton deleteLecturersButton = new JButton();
		JPanel lecturerAvailabilityTimesPanel = new JPanel();
		JScrollPane lecturerAvailabilityTimesScrollPane = new JScrollPane();
		lecturerAvailabilityTimesTable = new JTable();
		JPanel lecturerAvailabilityTimesOptionsPanel = new JPanel();
		JButton removeLecturerAvailabilityTimesButton = new JButton();
		JButton addLecturerAvailabilityTimesButton = new JButton();
		JPanel classesPoolPanel = new JPanel();
		JPanel classesEditAndViewPanel = new JPanel();
		JPanel saveClassesPanel = new JPanel();
		JLabel classesNameLabel = new JLabel();
		classesNameTextField = new JTextField();
		JLabel classesTypeLabel = new JLabel();
		classesTypeComboBox = new JComboBox();
		JButton saveClassesButton = new JButton();
		JScrollPane classesPoolScrollPane = new JScrollPane();
		classesPoolTable = new JTable();
		JPanel classesPoolOptionsPanel = new JPanel();
		JButton deleteSelectedClassesButton = new JButton();
		JPanel groupsPanel = new JPanel();
		JPanel groupEditAndViewPanel = new JPanel();
		JPanel saveGroupPanel = new JPanel();
		JLabel groupNameLabel = new JLabel();
		groupNameTextField = new JTextField();
		JLabel groupSizeLabel = new JLabel();
		groupSizeSpinner = new JSpinner();
		JLabel groupTypeLabel = new JLabel();
		groupTypeComboBox = new JComboBox();
		JButton saveGroupButton = new JButton();
		JScrollPane groupsScrollPane = new JScrollPane();
		groupsTable = new JTable();
		JPanel groupsOptionsPanel = new JPanel();
		JButton deleteSelectedGroupsButton = new JButton();
		JPanel roomsPanel = new JPanel();
		JPanel roomEditAndViewPanel = new JPanel();
		JPanel saveRoomPanel = new JPanel();
		JLabel roomNameLabel = new JLabel();
		roomNameTextField = new JTextField();
		JLabel roomCapacityLabel = new JLabel();
		roomCapacitySpinner = new JSpinner();
		JLabel roomTypeLabel = new JLabel();
		roomTypeComboBox = new JComboBox();
		JButton saveRoomButton = new JButton();
		JScrollPane roomsScrollPane = new JScrollPane();
		roomsTable = new JTable();
		JPanel groupsOptionsPanel2 = new JPanel();
		JButton deleteSelectedRoomsButton = new JButton();
		JPanel roomAvailabilityTimesPanel = new JPanel();
		JScrollPane roomAvailabilityTimesScrollPane = new JScrollPane();
		roomAvailabilityTimesTable = new JTable();
		JPanel roomAvailabilityTimesOptionsPanel = new JPanel();
		JButton removeRoomAvailabilityTimesButton = new JButton();
		JButton addRoomAvailabilityTimesButton = new JButton();
		JPanel physicalClasseslPanel = new JPanel();
		JPanel physicalClassesEditAndViewPanel = new JPanel();
		JPanel savePhysicalClassesPanel = new JPanel();
		JLabel classesLecturersLabel = new JLabel();
		JLabel classesGroupsLabel = new JLabel();
		JScrollPane classesLecturerScrollPane = new JScrollPane();
		classesLecturersList = new JList();
		JScrollPane classesGroupsScrollPane = new JScrollPane();
		classesGroupsList = new JList();
		JLabel ClassesClassesLabel = new JLabel();
		JScrollPane classesClassesScrollPane = new JScrollPane();
		classesClassesList = new JList();
		JButton savePhysicalClassesButton = new JButton();
		JScrollPane physicalClassesScrollPane = new JScrollPane();
		physicalClassesTable = new JTable();
		JPanel physicalClassesOptionsPanel = new JPanel();
		JButton deletePhysicalClassesButton = new JButton();
		JPanel availabilityTimesModificationPanel = new JPanel();
		JPanel availabilityTimesEditPanel = new JPanel();
		JLabel availabilityTimeStartLabel = new JLabel();
		availabilityTimeStartHourSpinner = new JSpinner();
		availabilityTimeStartMinuteSpinner = new JSpinner();
		JLabel availabilityTimeEndHourLabel = new JLabel();
		availabilityTimeEndHourSpinner = new JSpinner();
		availabilityTimeEndMinuteSpinner = new JSpinner();
		JButton addAvailabilityTimeButton = new JButton();
		JPanel weekdayPanel = new JPanel();
		JScrollPane weekdaysScrollPane = new JScrollPane();
		weekdaysList = new JList();
		JButton deleteAvailabilityTimesButton = new JButton();
		JPanel availabilityTimesPanel = new JPanel();
		JMenuBar dataBaseMenuBar = new JMenuBar();
		JMenu dataBaseMenu = new JMenu();
		JMenuItem persistMenuItem = new JMenuItem();
		JMenuItem loadMenuItem = new JMenuItem();
		JScrollPane availabilityTimesScrollPane = new JScrollPane();
		availabilityTimesTable = new JTable();
		lecturers = dataManager.lecturers;
		availabilityTimes = dataManager.availabilityTimes;
		selectedLecturer = null;
		TimeConverter timeConverter = new TimeConverter();
		classesTypes = dataManager.classesTypes;
		classesList = dataManager.classesList;
		groups = dataManager.groups;
		groupTypes = dataManager.groupTypes;
		rooms = dataManager.rooms;
		roomTypes = dataManager.roomTypes;
		selectedRoom = null;
		physicalClassesList = dataManager.physicalClassesList;
		weekdays = dataManager.weekdays;
		StringValidator stringValidator = new StringValidator();

		//======== this ========
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				thisWindowClosing(e);
			}
		});
		Container contentPane = getContentPane();
		contentPane.setLayout(new MigLayout(
			"insets 0,hidemode 3",
			"[631:527,left][300,fill]",
			"[358,grow,fill]"));

		//======== tabbedPane ========
		{

			//======== lecturersPanel ========
			{

				// JFormDesigner evaluation mark
				lecturersPanel.setBorder(new javax.swing.border.CompoundBorder(
					new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
						"JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
						javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
						java.awt.Color.red), lecturersPanel.getBorder())); lecturersPanel.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

				lecturersPanel.setLayout(new MigLayout(
					"hidemode 3",
					"[300,fill][237,fill]",
					"[542]"));

				//======== lecturerEditAndViewPanel ========
				{
					lecturerEditAndViewPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[fill]",
						"[195:135,top][328,center][127,bottom][13][][0]"));

					//======== saveLecturerPanel ========
					{
						saveLecturerPanel.setBorder(new TitledBorder("Utw\u00f3rz nowego prowadz\u0105cego"));
						saveLecturerPanel.setBackground(new Color(214, 217, 223));
						saveLecturerPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[119,fill][198,fill]",
							"[][][][][]"));

						//---- lecturerDegreeLabel ----
						lecturerDegreeLabel.setText("Stopie\u0144/Tytu\u0142:");
						saveLecturerPanel.add(lecturerDegreeLabel, "cell 0 0");
						saveLecturerPanel.add(lecturerDegreeTextField, "cell 1 0");

						//---- lecturerFstNameLabel ----
						lecturerFstNameLabel.setText("Imi\u0119:");
						saveLecturerPanel.add(lecturerFstNameLabel, "cell 0 1");
						saveLecturerPanel.add(lecturerFstNameTextField, "cell 1 1");

						//---- lecturerSndNameLabel ----
						lecturerSndNameLabel.setText("Nazwisko:");
						saveLecturerPanel.add(lecturerSndNameLabel, "cell 0 2");
						saveLecturerPanel.add(lecturerSndNameTextField, "cell 1 2");

						//---- saveLecturerButton ----
						saveLecturerButton.setText("Dodaj");
						saveLecturerButton.addActionListener(e -> saveLecturerButtonActionPerformed());
						saveLecturerPanel.add(saveLecturerButton, "cell 0 3");
					}
					lecturerEditAndViewPanel.add(saveLecturerPanel, "cell 0 0");

					//======== lecturersScrollPane ========
					{

						//---- lecturersTable ----
						lecturersTable.setCellSelectionEnabled(true);
						lecturersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						lecturersTable.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseClicked(MouseEvent e) {
								lecturersTableMouseClicked();
							}
						});
						lecturersScrollPane.setViewportView(lecturersTable);
					}
					lecturerEditAndViewPanel.add(lecturersScrollPane, "cell 0 1");

					//======== lecturerOptionsPanel ========
					{
						lecturerOptionsPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[81,center]"));

						//---- deleteLecturersButton ----
						deleteLecturersButton.setText("Usu\u0144 zaznaczonego prowadz\u0105cego");
						deleteLecturersButton.addActionListener(e -> deleteSelectedLecturersButtonActionPerformed());
						lecturerOptionsPanel.add(deleteLecturersButton, "cell 1 0");
					}
					lecturerEditAndViewPanel.add(lecturerOptionsPanel, "cell 0 2");
				}
				lecturersPanel.add(lecturerEditAndViewPanel, "cell 0 0");

				//======== lecturerAvailabilityTimesPanel ========
				{
					lecturerAvailabilityTimesPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[fill]",
						"[576,top][94,top][]"));

					//======== lecturerAvailabilityTimesScrollPane ========
					{
						lecturerAvailabilityTimesScrollPane.setViewportView(lecturerAvailabilityTimesTable);
					}
					lecturerAvailabilityTimesPanel.add(lecturerAvailabilityTimesScrollPane, "cell 0 0");

					//======== lecturerAvailabilityTimesOptionsPanel ========
					{
						lecturerAvailabilityTimesOptionsPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[][]"));

						//---- removeLecturerAvailabilityTimesButton ----
						removeLecturerAvailabilityTimesButton.setText("Usu\u0144 zaznaczone");
						removeLecturerAvailabilityTimesButton.addActionListener(e -> removeLecturerAvailabilityTimesButtonActionPerformed());
						lecturerAvailabilityTimesOptionsPanel.add(removeLecturerAvailabilityTimesButton, "cell 1 0");

						//---- addLecturerAvailabilityTimesButton ----
						addLecturerAvailabilityTimesButton.setText("Dodaj zaznaczone");
						addLecturerAvailabilityTimesButton.addActionListener(e -> addLecturerAvailabilityTimesButtonActionPerformed());
						lecturerAvailabilityTimesOptionsPanel.add(addLecturerAvailabilityTimesButton, "cell 1 0");
					}
					lecturerAvailabilityTimesPanel.add(lecturerAvailabilityTimesOptionsPanel, "cell 0 1");
				}
				lecturersPanel.add(lecturerAvailabilityTimesPanel, "cell 1 0");
			}
			tabbedPane.addTab("Prowadz\u0105cy", lecturersPanel);

			//======== classesPoolPanel ========
			{
				classesPoolPanel.setLayout(new MigLayout(
					"hidemode 3",
					"[600,fill]",
					"[542]"));

				//======== classesEditAndViewPanel ========
				{
					classesEditAndViewPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[633,fill]",
						"[195:135,top][374,center][127,top][13][][0]"));

					//======== saveClassesPanel ========
					{
						saveClassesPanel.setBorder(new TitledBorder("Utw\u00f3rz nowe zaj\u0119cia"));
						saveClassesPanel.setBackground(new Color(214, 217, 223));
						saveClassesPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[119,fill][198,fill]",
							"[][][][]"));

						//---- classesNameLabel ----
						classesNameLabel.setText("Nazwa:");
						saveClassesPanel.add(classesNameLabel, "cell 0 0");
						saveClassesPanel.add(classesNameTextField, "cell 1 0");

						//---- classesTypeLabel ----
						classesTypeLabel.setText("Typ zaj\u0119\u0107:");
						saveClassesPanel.add(classesTypeLabel, "cell 0 1");

						//---- classesTypeComboBox ----
						        classesTypeComboBox.setRenderer(new DefaultListCellRenderer()
						        {
						            @Override
						            public Component getListCellRendererComponent(
						                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
						            {
						                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
						                if (value instanceof ClassesType)
						                {
						                    ClassesType ct = (ClassesType) value;
						                    setText(ct.getName());
						                }
						                return this;
						            }
						        });
						saveClassesPanel.add(classesTypeComboBox, "cell 1 1");

						//---- saveClassesButton ----
						saveClassesButton.setText("Dodaj");
						saveClassesButton.addActionListener(e -> saveClassesButtonActionPerformed());
						saveClassesPanel.add(saveClassesButton, "cell 0 2");
					}
					classesEditAndViewPanel.add(saveClassesPanel, "cell 0 0");

					//======== classesPoolScrollPane ========
					{

						//---- classesPoolTable ----
						classesPoolTable.setCellSelectionEnabled(true);
						classesPoolTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						classesPoolScrollPane.setViewportView(classesPoolTable);
					}
					classesEditAndViewPanel.add(classesPoolScrollPane, "cell 0 1");

					//======== classesPoolOptionsPanel ========
					{
						classesPoolOptionsPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[73,bottom]"));

						//---- deleteSelectedClassesButton ----
						deleteSelectedClassesButton.setText("Usu\u0144 zaznaczone zaj\u0119cia");
						deleteSelectedClassesButton.addActionListener(e -> deleteSelectedClassesButtonActionPerformed());
						classesPoolOptionsPanel.add(deleteSelectedClassesButton, "cell 1 0");
					}
					classesEditAndViewPanel.add(classesPoolOptionsPanel, "cell 0 2");
				}
				classesPoolPanel.add(classesEditAndViewPanel, "cell 0 0");
			}
			tabbedPane.addTab("Pula zaj\u0119\u0107", classesPoolPanel);

			//======== groupsPanel ========
			{
				groupsPanel.setLayout(new MigLayout(
					"hidemode 3",
					"[635,fill]",
					"[542]"));

				//======== groupEditAndViewPanel ========
				{
					groupEditAndViewPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[684,fill]",
						"[195:135,top][329,center][127,top][13][][0]"));

					//======== saveGroupPanel ========
					{
						saveGroupPanel.setBorder(new TitledBorder("Utw\u00f3rz now\u0105 grup\u0119/rok"));
						saveGroupPanel.setBackground(new Color(214, 217, 223));
						saveGroupPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[119,fill][160,fill]",
							"[][30][30][]"));

						//---- groupNameLabel ----
						groupNameLabel.setText("Nazwa:");
						saveGroupPanel.add(groupNameLabel, "cell 0 0");
						saveGroupPanel.add(groupNameTextField, "cell 1 0");

						//---- groupSizeLabel ----
						groupSizeLabel.setText("Ilo\u015b\u0107 student\u00f3w:");
						saveGroupPanel.add(groupSizeLabel, "cell 0 1");

						//---- groupSizeSpinner ----
						groupSizeSpinner.setModel(new SpinnerNumberModel(30, 1, 260, 1));
						saveGroupPanel.add(groupSizeSpinner, "cell 1 1,aligny center,growy 0");

						//---- groupTypeLabel ----
						groupTypeLabel.setText("Typ grupy:");
						saveGroupPanel.add(groupTypeLabel, "cell 0 2");

						//---- groupTypeComboBox ----
						        groupTypeComboBox.setRenderer(new DefaultListCellRenderer()
						        {
						            @Override
						            public Component getListCellRendererComponent(
						                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
						            {
						                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
						                if (value instanceof GroupType)
						                {
						                    GroupType gt = (GroupType) value;
						                    setText(gt.getName());
						                }
						                return this;
						            }
						        });
						saveGroupPanel.add(groupTypeComboBox, "cell 1 2");

						//---- saveGroupButton ----
						saveGroupButton.setText("Dodaj");
						saveGroupButton.addActionListener(e -> saveGroupButtonActionPerformed());
						saveGroupPanel.add(saveGroupButton, "cell 0 3");
					}
					groupEditAndViewPanel.add(saveGroupPanel, "cell 0 0");

					//======== groupsScrollPane ========
					{

						//---- groupsTable ----
						groupsTable.setCellSelectionEnabled(true);
						groupsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						groupsScrollPane.setViewportView(groupsTable);
					}
					groupEditAndViewPanel.add(groupsScrollPane, "cell 0 1");

					//======== groupsOptionsPanel ========
					{
						groupsOptionsPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[73,bottom]"));

						//---- deleteSelectedGroupsButton ----
						deleteSelectedGroupsButton.setText("Usu\u0144 zaznaczon\u0105 grup\u0119");
						deleteSelectedGroupsButton.addActionListener(e -> deleteSelectedGroupsButtonActionPerformed());
						groupsOptionsPanel.add(deleteSelectedGroupsButton, "cell 1 0");
					}
					groupEditAndViewPanel.add(groupsOptionsPanel, "cell 0 2");
				}
				groupsPanel.add(groupEditAndViewPanel, "cell 0 0");
			}
			tabbedPane.addTab("Grupy", groupsPanel);

			//======== roomsPanel ========
			{
				roomsPanel.setLayout(new MigLayout(
					"hidemode 3",
					"[318,fill][330,fill]",
					"[542][]"));

				//======== roomEditAndViewPanel ========
				{
					roomEditAndViewPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[322,fill]",
						"[195:135,top][342,center][127,top][13][][0]"));

					//======== saveRoomPanel ========
					{
						saveRoomPanel.setBorder(new TitledBorder("Utw\u00f3rz now\u0105 sal\u0119"));
						saveRoomPanel.setBackground(new Color(214, 217, 223));
						saveRoomPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[119,fill][160,fill]",
							"[][][][][]"));

						//---- roomNameLabel ----
						roomNameLabel.setText("Nazwa:");
						saveRoomPanel.add(roomNameLabel, "cell 0 0");
						saveRoomPanel.add(roomNameTextField, "cell 1 0");

						//---- roomCapacityLabel ----
						roomCapacityLabel.setText("Pojemno\u015b\u0107:");
						saveRoomPanel.add(roomCapacityLabel, "cell 0 1");

						//---- roomCapacitySpinner ----
						roomCapacitySpinner.setModel(new SpinnerNumberModel(30, 1, 260, 1));
						saveRoomPanel.add(roomCapacitySpinner, "cell 1 1");

						//---- roomTypeLabel ----
						roomTypeLabel.setText("Typ sali:");
						saveRoomPanel.add(roomTypeLabel, "cell 0 2");

						//---- roomTypeComboBox ----
						        roomTypeComboBox.setRenderer(new DefaultListCellRenderer()
						        {
						            @Override
						            public Component getListCellRendererComponent(
						                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
						            {
						                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
						                if (value instanceof RoomType)
						                {
						                    RoomType rt = (RoomType) value;
						                    setText(rt.getName());
						                }
						                return this;
						            }
						        });
						saveRoomPanel.add(roomTypeComboBox, "cell 1 2");

						//---- saveRoomButton ----
						saveRoomButton.setText("Dodaj");
						saveRoomButton.addActionListener(e -> saveRoomButtonActionPerformed());
						saveRoomPanel.add(saveRoomButton, "cell 0 3");
					}
					roomEditAndViewPanel.add(saveRoomPanel, "cell 0 0");

					//======== roomsScrollPane ========
					{

						//---- roomsTable ----
						roomsTable.setCellSelectionEnabled(true);
						roomsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						roomsTable.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseClicked(MouseEvent e) {
								roomsTableMouseClicked();
							}
						});
						roomsScrollPane.setViewportView(roomsTable);
					}
					roomEditAndViewPanel.add(roomsScrollPane, "cell 0 1");

					//======== groupsOptionsPanel2 ========
					{
						groupsOptionsPanel2.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[73,bottom]"));

						//---- deleteSelectedRoomsButton ----
						deleteSelectedRoomsButton.setText("Usu\u0144 zaznaczon\u0105 sal\u0119");
						deleteSelectedRoomsButton.addActionListener(e -> deleteSelectedRoomsButtonActionPerformed());
						groupsOptionsPanel2.add(deleteSelectedRoomsButton, "cell 1 0");
					}
					roomEditAndViewPanel.add(groupsOptionsPanel2, "cell 0 2");
				}
				roomsPanel.add(roomEditAndViewPanel, "cell 0 0");

				//======== roomAvailabilityTimesPanel ========
				{
					roomAvailabilityTimesPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[fill]",
						"[517,top][94,top][]"));

					//======== roomAvailabilityTimesScrollPane ========
					{
						roomAvailabilityTimesScrollPane.setViewportView(roomAvailabilityTimesTable);
					}
					roomAvailabilityTimesPanel.add(roomAvailabilityTimesScrollPane, "cell 0 0");

					//======== roomAvailabilityTimesOptionsPanel ========
					{
						roomAvailabilityTimesOptionsPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[][]"));

						//---- removeRoomAvailabilityTimesButton ----
						removeRoomAvailabilityTimesButton.setText("Usu\u0144 zaznaczone");
						removeRoomAvailabilityTimesButton.addActionListener(e -> removeRoomAvailabilityTimesButtonActionPerformed());
						roomAvailabilityTimesOptionsPanel.add(removeRoomAvailabilityTimesButton, "cell 1 0");

						//---- addRoomAvailabilityTimesButton ----
						addRoomAvailabilityTimesButton.setText("Dodaj zaznaczone");
						addRoomAvailabilityTimesButton.addActionListener(e -> addRoomAvailabilityTimesButtonActionPerformed());
						roomAvailabilityTimesOptionsPanel.add(addRoomAvailabilityTimesButton, "cell 1 0");
					}
					roomAvailabilityTimesPanel.add(roomAvailabilityTimesOptionsPanel, "cell 0 1");
				}
				roomsPanel.add(roomAvailabilityTimesPanel, "cell 1 0");
			}
			tabbedPane.addTab("Sale", roomsPanel);

			//======== physicalClasseslPanel ========
			{
				physicalClasseslPanel.setLayout(new MigLayout(
					"hidemode 3",
					"[706,fill]",
					"[542]"));

				//======== physicalClassesEditAndViewPanel ========
				{
					physicalClassesEditAndViewPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[678,fill]",
						"[195:530,top][343,center][127,top][13][][0]"));

					//======== savePhysicalClassesPanel ========
					{
						savePhysicalClassesPanel.setBorder(new TitledBorder("Utw\u00f3rz nowe zaj\u0119cia"));
						savePhysicalClassesPanel.setBackground(new Color(214, 217, 223));
						savePhysicalClassesPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[460,fill][460,fill]",
							"[][118][][125]"));

						//---- classesLecturersLabel ----
						classesLecturersLabel.setText("Wybierz prowadz\u0105cego:");
						savePhysicalClassesPanel.add(classesLecturersLabel, "cell 0 0");

						//---- classesGroupsLabel ----
						classesGroupsLabel.setText("Wybierz grup\u0119/rok:");
						savePhysicalClassesPanel.add(classesGroupsLabel, "cell 1 0");

						//======== classesLecturerScrollPane ========
						{

							//---- classesLecturersList ----
							        classesLecturersList.setCellRenderer(new DefaultListCellRenderer()
							        {
							            @Override
							            public Component getListCellRendererComponent(
							                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
							            {
							                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
							                if (value instanceof Lecturer)
							                {
							                    Lecturer l = (Lecturer) value;
							                    setText(l.getFirstName() + " " + l.getLastName());
							                }
							                return this;
							            }
							        });
							classesLecturersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
							classesLecturerScrollPane.setViewportView(classesLecturersList);
						}
						savePhysicalClassesPanel.add(classesLecturerScrollPane, "cell 0 1");

						//======== classesGroupsScrollPane ========
						{

							//---- classesGroupsList ----
							        classesGroupsList.setCellRenderer(new DefaultListCellRenderer()
							        {
							            @Override
							            public Component getListCellRendererComponent(
							                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
							            {
							                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
							                if (value instanceof Group)
							                {
							                    Group g = (Group) value;
							                    setText(g.getName() + ", " + g.getGroupType().getName());
							                }
							                return this;
							            }
							        });
							classesGroupsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
							classesGroupsScrollPane.setViewportView(classesGroupsList);
						}
						savePhysicalClassesPanel.add(classesGroupsScrollPane, "cell 1 1");

						//---- ClassesClassesLabel ----
						ClassesClassesLabel.setText("Wybierz baz\u0119 zaj\u0119\u0107:");
						savePhysicalClassesPanel.add(ClassesClassesLabel, "cell 0 2");

						//======== classesClassesScrollPane ========
						{

							//---- classesClassesList ----
							        classesClassesList.setCellRenderer(new DefaultListCellRenderer()
							        {
							            @Override
							            public Component getListCellRendererComponent(
							                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
							            {
							                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
							                if (value instanceof Classes)
							                {
							                    Classes c = (Classes) value;
							                    setText(c.getName() + ", " + c.getClassesType().getName());
							                }
							                return this;
							            }
							        });
							classesClassesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
							classesClassesScrollPane.setViewportView(classesClassesList);
						}
						savePhysicalClassesPanel.add(classesClassesScrollPane, "cell 0 3");

						//---- savePhysicalClassesButton ----
						savePhysicalClassesButton.setText("Dodaj");
						savePhysicalClassesButton.addActionListener(e -> savePhysicalClassesButtonActionPerformed());
						savePhysicalClassesPanel.add(savePhysicalClassesButton, "cell 1 3,align center center,grow 0 0");
					}
					physicalClassesEditAndViewPanel.add(savePhysicalClassesPanel, "cell 0 0");

					//======== physicalClassesScrollPane ========
					{

						//---- physicalClassesTable ----
						physicalClassesTable.setCellSelectionEnabled(true);
						physicalClassesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						physicalClassesScrollPane.setViewportView(physicalClassesTable);
					}
					physicalClassesEditAndViewPanel.add(physicalClassesScrollPane, "cell 0 1");

					//======== physicalClassesOptionsPanel ========
					{
						physicalClassesOptionsPanel.setLayout(new MigLayout(
							"hidemode 3",
							"[fill][fill][fill]",
							"[49,bottom]"));

						//---- deletePhysicalClassesButton ----
						deletePhysicalClassesButton.setText("Usu\u0144 zaznaczone zaj\u0119cia");
						deletePhysicalClassesButton.addActionListener(e -> deletePhysicalClassesButtonActionPerformed());
						physicalClassesOptionsPanel.add(deletePhysicalClassesButton, "cell 1 0");
					}
					physicalClassesEditAndViewPanel.add(physicalClassesOptionsPanel, "cell 0 2");
				}
				physicalClasseslPanel.add(physicalClassesEditAndViewPanel, "cell 0 0");
			}
			tabbedPane.addTab("Zaj\u0119cia", physicalClasseslPanel);

			//======== availabilityTimesModificationPanel ========
			{
				availabilityTimesModificationPanel.setLayout(new MigLayout(
					"hidemode 3",
					"[336,fill][fill]",
					"[fill][]"));

				//======== availabilityTimesEditPanel ========
				{
					availabilityTimesEditPanel.setBorder(new TitledBorder("Edytuj godziny dost\u0119pno\u015bci"));
					availabilityTimesEditPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[76,fill][60,fill][60,fill]",
						"[][][]"));

					//---- availabilityTimeStartLabel ----
					availabilityTimeStartLabel.setText("Od:");
					availabilityTimesEditPanel.add(availabilityTimeStartLabel, "cell 0 0");

					//---- availabilityTimeStartHourSpinner ----
					availabilityTimeStartHourSpinner.setModel(new SpinnerNumberModel(8, 0, 23, 1));
					availabilityTimesEditPanel.add(availabilityTimeStartHourSpinner, "cell 1 0");

					//---- availabilityTimeStartMinuteSpinner ----
					availabilityTimeStartMinuteSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
					availabilityTimesEditPanel.add(availabilityTimeStartMinuteSpinner, "cell 2 0");

					//---- availabilityTimeEndHourLabel ----
					availabilityTimeEndHourLabel.setText("Do:");
					availabilityTimesEditPanel.add(availabilityTimeEndHourLabel, "cell 0 1");

					//---- availabilityTimeEndHourSpinner ----
					availabilityTimeEndHourSpinner.setModel(new SpinnerNumberModel(8, 0, 23, 1));
					availabilityTimesEditPanel.add(availabilityTimeEndHourSpinner, "cell 1 1");

					//---- availabilityTimeEndMinuteSpinner ----
					availabilityTimeEndMinuteSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
					availabilityTimesEditPanel.add(availabilityTimeEndMinuteSpinner, "cell 2 1");

					//---- addAvailabilityTimeButton ----
					addAvailabilityTimeButton.setText("Dodaj");
					addAvailabilityTimeButton.addActionListener(e -> addAvailabilityTimeButtonActionPerformed());
					availabilityTimesEditPanel.add(addAvailabilityTimeButton, "cell 1 2");
				}
				availabilityTimesModificationPanel.add(availabilityTimesEditPanel, "cell 0 0");

				//======== weekdayPanel ========
				{
					weekdayPanel.setBorder(new TitledBorder("Wybierz dni tygodnia"));
					weekdayPanel.setLayout(new MigLayout(
						"hidemode 3",
						"[333,fill]",
						"[]"));

					//======== weekdaysScrollPane ========
					{
						weekdaysScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

						//---- weekdaysList ----
						        weekdaysList.setCellRenderer(new DefaultListCellRenderer()
						        {
						            @Override
						            public Component getListCellRendererComponent(
						                    JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
						            {
						                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
						                if (value instanceof Weekday)
						                {
						                    Weekday weekday = (Weekday) value;
						                    setText(weekday.getName());
						                }
						                return this;
						            }
						        });
						weekdaysScrollPane.setViewportView(weekdaysList);
					}
					weekdayPanel.add(weekdaysScrollPane, "cell 0 0");
				}
				availabilityTimesModificationPanel.add(weekdayPanel, "cell 1 0");

				//---- deleteAvailabilityTimesButton ----
				deleteAvailabilityTimesButton.setText("Usu\u0144 zaznaczone godziny dost\u0119pno\u015bci");
				deleteAvailabilityTimesButton.addActionListener(e -> deleteAvailabilityTimesButtonActionPerformed());
				availabilityTimesModificationPanel.add(deleteAvailabilityTimesButton, "cell 1 1,alignx right,growx 0");
			}
			tabbedPane.addTab("Godziny dost\u0119pno\u015bci", availabilityTimesModificationPanel);
		}
		contentPane.add(tabbedPane, "cell 0 0");

		//======== availabilityTimesPanel ========
		{
			availabilityTimesPanel.setLayout(new MigLayout(
				"hidemode 3",
				"[307,right]",
				"[26:30,fill][578,fill]"));

			//======== dataBaseMenuBar ========
			{

				//======== dataBaseMenu ========
				{
					dataBaseMenu.setText("Baza danych");

					//---- persistMenuItem ----
					persistMenuItem.setText("Zapisz");
					persistMenuItem.addActionListener(e -> persistMenuItemActionPerformed());
					dataBaseMenu.add(persistMenuItem);

					//---- loadMenuItem ----
					loadMenuItem.setText("Wczytaj");
					loadMenuItem.addActionListener(e -> loadMenuItemActionPerformed());
					dataBaseMenu.add(loadMenuItem);
				}
				dataBaseMenuBar.add(dataBaseMenu);
			}
			availabilityTimesPanel.add(dataBaseMenuBar, "cell 0 0,alignx left,growx 0");

			//======== availabilityTimesScrollPane ========
			{
				availabilityTimesScrollPane.setViewportView(availabilityTimesTable);
			}
			availabilityTimesPanel.add(availabilityTimesScrollPane, "cell 0 1");
		}
		contentPane.add(availabilityTimesPanel, "cell 1 0");
		setSize(945, 600);
		setLocationRelativeTo(getOwner());

		//---- bindings ----
		BindingGroup lecturersBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				lecturers, lecturersTable, "table");
			JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("degree"))
				.setColumnName("Stopie\u0144/Tytu\u0142")
				.setColumnClass(String.class);
			columnBinding.setSourceNullValue("N/D");
			columnBinding.setSourceUnreadableValue("N/D");
			columnBinding.setValidator(stringValidator);
			columnBinding = binding.addColumnBinding(BeanProperty.create("firstName"))
				.setColumnName("Imi\u0119")
				.setColumnClass(String.class);
			columnBinding.setSourceNullValue("N/D");
			columnBinding.setSourceUnreadableValue("N/D");
			columnBinding.setValidator(stringValidator);
			columnBinding = binding.addColumnBinding(BeanProperty.create("lastName"))
				.setColumnName("Nazwisko")
				.setColumnClass(String.class);
			columnBinding.setSourceNullValue("N/D");
			columnBinding.setSourceUnreadableValue("N/D");
			columnBinding.setValidator(stringValidator);
			lecturersBindingGroup.addBinding(binding);
			binding.bind();
		}
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				physicalClassesList, physicalClassesTable);
			binding.setEditable(false);
			JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("classes.name"))
				.setColumnName("Zaj\u0119cia")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setSourceNullValue("N/A");
			columnBinding = binding.addColumnBinding(BeanProperty.create("classes.classesType.name"))
				.setColumnName("Typ zaj\u0119\u0107")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setSourceNullValue("N/A");
			binding.addColumnBinding(ELProperty.create("${lecturer.firstName} ${lecturer.lastName}"))
				.setColumnName("Prowadz\u0105cy")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding = binding.addColumnBinding(BeanProperty.create("group.name"))
				.setColumnName("Grupa")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setSourceNullValue("N/A");
			binding.addColumnBinding(BeanProperty.create("group.groupType.name"))
				.setColumnName("Typ grupy")
				.setColumnClass(String.class)
				.setEditable(false);
			lecturersBindingGroup.addBinding(binding);
		}
		lecturersBindingGroup.addBinding(SwingBindings.createJListBinding(UpdateStrategy.READ_WRITE,
			weekdays, weekdaysList));
		lecturersBindingGroup.bind();
		BindingGroup availabilityTimesBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				availabilityTimes, availabilityTimesTable);
			binding.setEditable(false);
			JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("start"))
				.setColumnName("Od")
				.setColumnClass(String.class);
			columnBinding.setConverter(timeConverter);
			columnBinding = binding.addColumnBinding(BeanProperty.create("end"))
				.setColumnName("Do")
				.setColumnClass(String.class);
			columnBinding.setConverter(timeConverter);
			binding.addColumnBinding(BeanProperty.create("weekday.name"))
				.setColumnName("Dzie\u0144")
				.setColumnClass(String.class);
			availabilityTimesBindingGroup.addBinding(binding);
		}
		availabilityTimesBindingGroup.bind();
		BindingGroup lecturerAvailabilityTimesBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				selectedLecturer, (BeanProperty) BeanProperty.create("lecturerAvailabilityTimes"), lecturerAvailabilityTimesTable, "selectedLecturer");
			JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.start"))
				.setColumnName("Od")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setConverter(timeConverter);
			columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.end"))
				.setColumnName("Do")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setConverter(timeConverter);
			binding.addColumnBinding(BeanProperty.create("availabilityTime.weekday.name"))
				.setColumnName("Dzie\u0144")
				.setColumnClass(String.class)
				.setEditable(false);
			binding.addColumnBinding(BeanProperty.create("available"))
				.setColumnName("Dost\u0119pny?")
				.setColumnClass(Boolean.class);
			lecturerAvailabilityTimesBindingGroup.addBinding(binding);
			binding.bind();
		}
		lecturerAvailabilityTimesBindingGroup.bind();
		BindingGroup classesTypesBindingGroup = new BindingGroup();
		classesTypesBindingGroup.addBinding(SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE,
			classesTypes, classesTypeComboBox));
		classesTypesBindingGroup.bind();
		BindingGroup classesPoolBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				classesList, classesPoolTable);
			binding.addColumnBinding(BeanProperty.create("name"))
				.setColumnName("Nazwa")
				.setColumnClass(String.class);
			binding.addColumnBinding(BeanProperty.create("classesType.name"))
				.setColumnName("Rodzaj")
				.setColumnClass(String.class);
			classesPoolBindingGroup.addBinding(binding);
		}
		classesPoolBindingGroup.bind();
		BindingGroup groupsBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				groups, groupsTable);
			binding.addColumnBinding(BeanProperty.create("name"))
				.setColumnName("Nazwa")
				.setColumnClass(String.class);
			binding.addColumnBinding(BeanProperty.create("size"))
				.setColumnName("Ilo\u015b\u0107 student\u00f3w")
				.setColumnClass(Integer.class);
			binding.addColumnBinding(BeanProperty.create("groupType.name"))
				.setColumnName("Typ grupy")
				.setColumnClass(String.class)
				.setEditable(false);
			groupsBindingGroup.addBinding(binding);
		}
		groupsBindingGroup.bind();
		BindingGroup groupTypesBindingGroup = new BindingGroup();
		groupTypesBindingGroup.addBinding(SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE,
			groupTypes, groupTypeComboBox));
		groupTypesBindingGroup.bind();
		BindingGroup roomTypesBindingGroup = new BindingGroup();
		roomTypesBindingGroup.addBinding(SwingBindings.createJComboBoxBinding(UpdateStrategy.READ_WRITE,
			roomTypes, roomTypeComboBox));
		roomTypesBindingGroup.bind();
		BindingGroup roomBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				rooms, roomsTable);
			binding.addColumnBinding(BeanProperty.create("name"))
				.setColumnName("Nazwa")
				.setColumnClass(String.class);
			binding.addColumnBinding(BeanProperty.create("capacity"))
				.setColumnName("Pojemno\u015b\u0107")
				.setColumnClass(Integer.class);
			binding.addColumnBinding(BeanProperty.create("roomType.name"))
				.setColumnName("Typ sali")
				.setColumnClass(String.class)
				.setEditable(false);
			roomBindingGroup.addBinding(binding);
		}
		roomBindingGroup.bind();
		BindingGroup roomAvailabilityTimesBindingGroup = new BindingGroup();
		{
			JTableBinding binding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE,
				selectedRoom, (BeanProperty) BeanProperty.create("roomAvailabilityTimes"), roomAvailabilityTimesTable);
			JTableBinding.ColumnBinding columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.start"))
				.setColumnName("Od")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setConverter(timeConverter);
			columnBinding = binding.addColumnBinding(BeanProperty.create("availabilityTime.end"))
				.setColumnName("Do")
				.setColumnClass(String.class)
				.setEditable(false);
			columnBinding.setConverter(timeConverter);
			binding.addColumnBinding(BeanProperty.create("availabilityTime.weekday.name"))
				.setColumnName("Dzie\u0144")
				.setColumnClass(String.class)
				.setEditable(false);
			binding.addColumnBinding(BeanProperty.create("available"))
				.setColumnName("Wolny?")
				.setColumnClass(Boolean.class);
			roomAvailabilityTimesBindingGroup.addBinding(binding);
		}
		roomAvailabilityTimesBindingGroup.bind();
		BindingGroup physicalClassesBindingGroup = new BindingGroup();
		physicalClassesBindingGroup.addBinding(SwingBindings.createJListBinding(UpdateStrategy.READ_WRITE,
			lecturers, classesLecturersList));
		physicalClassesBindingGroup.addBinding(SwingBindings.createJListBinding(UpdateStrategy.READ_WRITE,
			classesList, classesClassesList));
		physicalClassesBindingGroup.addBinding(SwingBindings.createJListBinding(UpdateStrategy.READ_WRITE,
			groups, classesGroupsList));
		physicalClassesBindingGroup.bind();
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Nicholas Wilde
	private JTextField lecturerDegreeTextField;
	private JTextField lecturerFstNameTextField;
	private JTextField lecturerSndNameTextField;
	private JTable lecturersTable;
	private JTable lecturerAvailabilityTimesTable;
	private JTextField classesNameTextField;
	private JComboBox classesTypeComboBox;
	private JTable classesPoolTable;
	private JTextField groupNameTextField;
	private JSpinner groupSizeSpinner;
	private JComboBox groupTypeComboBox;
	private JTable groupsTable;
	private JTextField roomNameTextField;
	private JSpinner roomCapacitySpinner;
	private JComboBox roomTypeComboBox;
	private JTable roomsTable;
	private JTable roomAvailabilityTimesTable;
	private JList classesLecturersList;
	private JList classesGroupsList;
	private JList classesClassesList;
	private JTable physicalClassesTable;
	private JSpinner availabilityTimeStartHourSpinner;
	private JSpinner availabilityTimeStartMinuteSpinner;
	private JSpinner availabilityTimeEndHourSpinner;
	private JSpinner availabilityTimeEndMinuteSpinner;
	private JList weekdaysList;
	private JTable availabilityTimesTable;
	private EntityList<entity.Lecturer> lecturers;
	private EntityList<entity.AvailabilityTime> availabilityTimes;
	private Lecturer selectedLecturer;
	private EntityList<entity.ClassesType> classesTypes;
	private EntityList<entity.Classes> classesList;
	private EntityList<entity.Group> groups;
	private EntityList<entity.GroupType> groupTypes;
	private EntityList<entity.Room> rooms;
	private EntityList<entity.RoomType> roomTypes;
	private Room selectedRoom;
	private EntityList<entity.PhysicalClasses> physicalClassesList;
	private EntityList<entity.Weekday> weekdays;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
