package generate;

import entity.AvailabilityTime;
import entity.Weekday;
import singletons.DataManager;

import java.sql.Time;

/**
 * Klasa Input odpowiada za wczytanie listy zajęć, sal, prowadzących oraz grup
 * wraz z godzinami dostępności ww.
 */
public class Input {


    /**Tablica przechowująca wszystkich wczytanych prowądzących wraz z ich dostępnościami */
    public AvailabilityClass[] teacherTab;

    /**Tablice przechowujące wszystkie sale wraz z ich podziałem oraz dosępnością */
    public AvailabilityClass[] roomTabLab, roomTabAudt, roomTabLecture, roomTab;

    /**Tablica przechowująca wszystkie wczytane grupy wraz z ich dostępnościami */
    public AvailabilityClass[] groupTab;

    /** Tablica przechowująca wszystkie wczytane zajęcia*/
    public Zajecia [] listClasses;

    /** Zmienne pomocnicze: liczby sal określonego typu, oraz licznik chodzenia tablicach sal określonego typu*/
    private int Lab = 0, Aud = 0, Lecture = 0, var = 0;

    /**Obiekt zawierający dane wejściowe utworzony przez Grupę I */
    private DataManager dataManager;

    /**
     *Konstruktor przyjmuję obiekt Klasy DataManager, inicjalizuję tablicę klasy AvailabilityClass,
     * i przypisuję do nich dane
     *
     * @param data Jest to obiekt Klasy DataManager utworzoną przez programiste Grupy nr I
     *             Obekt ten zawiera ArrayListy z wszystkimi danymi potrzebnymi do generowania planu
     */
    public Input(DataManager data) {
        this.dataManager = data;
        teacherTab = new AvailabilityClass[dataManager.lecturers.size()];


        for (int i = 0; i < dataManager.lecturers.size(); i++) {


            teacherTab[i] = new AvailabilityClass();
            teacherTab[i].setId(dataManager.lecturers.get(i).getId());
            teacherTab[i].setName(dataManager.lecturers.get(i).getFirstName() + " " + dataManager.lecturers.get(i).getLastName());

            for(int j = 0; j< dataManager.lecturers.get(i).getLecturerAvailabilityTimes().size(); j++){

                teacherTab[i].availableTab
                        [convertDay(dataManager.lecturers.get(i).getLecturerAvailabilityTimes().get(j).getAvailabilityTime().getWeekday().getName())]
                        [convertTime(dataManager.lecturers.get(i).getLecturerAvailabilityTimes().get(j).getAvailabilityTime().getStart())] = true;
            }

        }




        for (int  i = 0; i <  dataManager.rooms.size(); i++){


            if(dataManager.rooms.get(i).getRoomType().getName().equals("Audytoryjne") || dataManager.rooms.get(i).getRoomType().getName().equals("Projektowe") ){
                Aud++;
            }else if(dataManager.rooms.get(i).getRoomType().getName().equals("Aula") || dataManager.rooms.get(i).getRoomType().getName().equals("Sala wykładowa") ){
                Lecture++;
            }else if(dataManager.rooms.get(i).getRoomType().getName().equals("Laboratoryjne")){
                Lab++;
            }
        }

        roomTabLab = new AvailabilityClass[Lab];
        roomTabAudt = new AvailabilityClass[Aud];
        roomTabLecture = new AvailabilityClass[Lecture];

        var = 0;
        for (int  i = 0; i <  dataManager.rooms.size(); i++){

            if(dataManager.rooms.get(i).getRoomType().getName().equals("Audytoryjne") || dataManager.rooms.get(i).getRoomType().getName().equals("Projektowe") ) {

                roomTabAudt[var] = new AvailabilityClass();
                roomTabAudt[var].setId(dataManager.rooms.get(i).getId());
                roomTabAudt[var].setName(dataManager.rooms.get(i).getName());
                roomTabAudt[var].setType(dataManager.rooms.get(i).getRoomType().getName());

                for (int j = 0; j < dataManager.rooms.get(i).getRoomAvailabilityTimes().size(); j++) {

                    roomTabAudt[var].availableTab
                            [convertDay(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getWeekday().getName())]
                            [convertTime(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getStart())] = true;
                 }
                 var++;
                }
            }
        var=0;
        for (int  i = 0; i <  dataManager.rooms.size(); i++) {

            if (dataManager.rooms.get(i).getRoomType().getName().equals("Aula") || dataManager.rooms.get(i).getRoomType().getName().equals("Sala wykładowa")) {

                roomTabLecture[var] = new AvailabilityClass();
                roomTabLecture[var].setId(dataManager.rooms.get(i).getId());
                roomTabLecture[var].setName(dataManager.rooms.get(i).getName());
                roomTabLecture[var].setType(dataManager.rooms.get(i).getRoomType().getName());
                for (int j = 0; j < dataManager.rooms.get(i).getRoomAvailabilityTimes().size(); j++) {

                    roomTabLecture[var].availableTab
                            [convertDay(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getWeekday().getName())]
                            [convertTime(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getStart())] = true;
                }
                var++;
            }
        }

        var=0;
        for (int  i = 0; i <  dataManager.rooms.size(); i++) {

            if (dataManager.rooms.get(i).getRoomType().getName().equals("Laboratoryjne")) {
                roomTabLab[var] = new AvailabilityClass();
                roomTabLab[var].setId(dataManager.rooms.get(i).getId());
                roomTabLab[var].setName(dataManager.rooms.get(i).getName());
                roomTabLab[var].setType(dataManager.rooms.get(i).getRoomType().getName());
                for (int j = 0; j < dataManager.rooms.get(i).getRoomAvailabilityTimes().size(); j++) {

                    roomTabLab[var].availableTab
                            [convertDay(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getWeekday().getName())]
                            [convertTime(dataManager.rooms.get(i).getRoomAvailabilityTimes().get(j).getAvailabilityTime().getStart())] = true;
                }
                var++;
            }
        }
        groupTab = new AvailabilityClass[dataManager.groups.size()];

        for (int  i = 0; i <  dataManager.groups.size(); i++){


            groupTab[i] = new AvailabilityClass();
            groupTab[i].setId(dataManager.groups.get(i).getId());
            groupTab[i].setName(dataManager.groups.get(i).getName());
            groupTab[i].setType(dataManager.groups.get(i).getGroupType().getName());


        }

        for(int i = 0; i < groupTab.length; i++){
            groupTab[i].setLocalID(i);
        }

        listClasses = new Zajecia[dataManager.physicalClassesList.size()];
        for(int i = 0; i <  dataManager.physicalClassesList.size(); i++){

            listClasses[i] = new Zajecia(
                    dataManager.physicalClassesList.get(i).getClasses().getName(),
                    dataManager.physicalClassesList.get(i).getLecturer().getId(),
                    dataManager.physicalClassesList.get(i).getClasses().getClassesType().getName(),
                    dataManager.physicalClassesList.get(i).getGroup().getName()
            );
        }

        }

    /**
     * Funkcja przyjmuję godzinę rozpoczęcia i konwertuję ją do numeru indeksu używanego do poruszania się po tablicy
     * @param start Cytowanie: https://docs.oracle.com/javase/7/docs/api/java/sql/Time.html
     * @return numer indeksu
     */

    public int convertTime(Time start){
        if(start.equals(new Time(8,0,0))){
            return 0;
        }else if(start.equals(new Time(9,45,0))){
            return 1;
        }else if(start.equals(new Time(11,30,0))){
            return 2;
        }else if(start.equals(new Time(13,15,0))){
            return 3;
        }else if(start.equals(new Time(15,00,0))){
            return 4;
        }else if(start.equals(new Time(16,45,0))){
            return 5;
        }else if(start.equals(new Time(18,30,0))){
            return 6;
        }else if(start.equals(new Time(20,15,0))){
            return 7;
        }else{
            return -1;
        }
    }

    /**
     * Funkcja przyjmuję Stringa z nazwą dnia tygodnia i konwertuję ją do numeru indeksu użwyanego do poruszania się po tablicy
     * @param day Cytowanie: https://docs.oracle.com/javase/7/docs/api/java/lang/String.html
     * @return numer indeksu
     */
    public int convertDay(String day){
        if(day.equals("Poniedziałek")){
            return 0;
        }else if(day.equals("Wtorek")){
            return 1;
        }else if(day.equals("Środa")){
            return 2;
        }else if(day.equals("Czwartek")){
            return 3;
        }else if(day.equals("Piątek")){
            return 4;
        }else {
            return 5;
        }
    }

}
