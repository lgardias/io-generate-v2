package generate;

/**
 * Klasa służaca za kontener informacji o dosepności danego obiektu w tygodniu
 * Created by HP on 2017-01-11.
 */
public class AvailabilityClass {

    /**Nazwa obiektu */
    private String name;

    /**Id obiektu */
    private int id;

    /**Id lokalne obiektu */
    private int localID;

    /** Rodzaj obiektu */
    private String type;

    /**Tablica dosepności obiektu [dni][godziny] */
    public boolean [][] availableTab = new boolean[6][8];


    /**
     * Konstruktor domyślny służacy do tworzenia pustego obiektu
     */
    public AvailabilityClass(){

        this.name = "null";
        this.id = -1;
        this.type = "";
        this.localID = -1;
        for (int i = 0; i < availableTab.length; i++){
            for(int j = 0; j < availableTab[i].length; j++){
                availableTab[i][j] = false;
            }
        }
    }

    /**
     * Metoda ustawiająca flagę dostępności danego obiektu
     * @param x indeks określający dzień tygodnia
     * @param y indeks określający godzinę rozpocżecia zajęć
     * @param flag flaga określająca czy obiekt jest dostępny w dniu x o godzinie y
     */
    void setFlag(int x,int y, boolean flag) {
            this.availableTab[x][y]=flag;

    }


    public int getLocalID() {
        return localID;
    }

    public void setLocalID(int localID) {
        this.localID = localID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
