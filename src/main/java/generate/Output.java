package generate;

import entity.util.EntityList;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Time;

/**
 * Klasa odpowiadająca za ekport gotowego planu do pliku .JSON
 * Cytowanie 1. JSON - https://docs.oracle.com/database/121/ADXDB/json.htm#ADXDB6250
 * Cytowanie 2. JSON - https://www.tutorialspoint.com/json/json_java_example.htm
 * Cytowanie 3. ArrayLIST - https://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html
 * Created by HP on 2017-01-11.
 */
public class Output {

    /**Tablica rzchowująca wygenerowany plan zajęć */
    public Zajecia [][][] schedule;

    /**Lista zajęć gotowych do wypisania  utworzona na podstawie klasy EntityListy autorstwa Karola Domańskiego (Grupa I) */
    public EntityList<Zajecia> zajecia;


    /**
     * Metoda przypisująca do Listy zajęcia danego prowadzącego
     * @param idTeacher id prowadzącego którego zajęcia chcemy wyeksportować
     */
    public void findTeacherPlan(int idTeacher)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule.length;i++)
        {
            for(int j=0;j<schedule[i].length;j++)
            {
                for(int k=0;k<schedule[i][j].length;k++)
                {
                    if(schedule[i][j][k].isFlag()==false) {
                        if (schedule[i][j][k].getTeacher() == idTeacher) {
                            schedule[i][j][k].day=j;
                            schedule[i][j][k].hour=k;
                             zajecia.add(schedule[i][j][k]);


                        }
                    }
                }
            }
        }
        saveToJson(zajecia);
    }

    /**
     * Metoda przypisująca do Listy zajęcia danej grupy
     * @param idGroup id grupy której zajęcia chcemy wyeksportować
     */
    public void findGroupPlan(int idGroup)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule[idGroup].length;i++)
        {
            for(int j=0;j<schedule[idGroup][i].length;j++)
            {
                if(schedule[idGroup][i][j].isFlag()== false)
                {
                    schedule[idGroup][i][j].day=i;
                    schedule[idGroup][i][j].hour=j;
                    zajecia.add(schedule[idGroup][i][j]);
                }
                System.out.println(schedule[idGroup][i][j].isFlag());
            }
        }
        saveToJson(zajecia);
    }


    /**
     * Metoda przypisująca do Listy zajęcia odbywające się w danej sali
     * @param idRoom id sali której zajęcia chcemy wyeksportować
     */
    public void findRoomPlan(int idRoom)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule.length;i++)
        {
            for(int j=0;j<schedule[i].length;j++)
            {
                for(int k=0;k<schedule[i][j].length;k++)
                {
                    if(schedule[i][j][k].isFlag()==false) {
                        if (schedule[i][j][k].getRoom() == idRoom) {
                            schedule[i][j][k].day=j;
                            schedule[i][j][k].hour=k;
                            zajecia.add(schedule[i][j][k]);
                        }
                    }
                }
            }
        }
        saveToJson(zajecia);
    }
    /**
     * Metoda przypisująca do Listy zajęcia jednego przedmotu
     * @param name inazwa zajęc które chcemy weksportować
     */
    public void findClassesPlan(String name)
    {
        zajecia = new EntityList<Zajecia>();
        for(int i=0;i<schedule.length;i++)
        {
            for(int j=0;j<schedule[i].length;j++)
            {
                for(int k=0;k<schedule[i][j].length;k++)
                {
                    if(schedule[i][j][k].isFlag()==false) {
                        if (schedule[i][j][k].getName().equals(name)) {
                            schedule[i][j][k].day=j;
                            schedule[i][j][k].hour=k;
                            zajecia.add(schedule[i][j][k]);
                        }
                    }
                }
            }
        }
        saveToJson(zajecia);
    }

    /**
     * Konstruktor inicjalizujący obiekt
     * @param plan tablica 3D z wygenerowanym planem zajęć
     */
    public Output(Zajecia [][][] plan)
    {
        this.schedule=plan;
    }

    /**
     * Metoda zapisująca Listę zajęć do pliku .JSON
     * @param result Lista EntityList( autorstwa Karola Domańskiego) zawięrajca dane gotowe do eksportu
     */
    public void saveToJson(EntityList<Zajecia> result)
    {
        PrintWriter zapis = null;
        try {
            zapis = new PrintWriter("output.txt");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for(int i=0;i<result.size();i++) {
            JSONObject obj = new JSONObject();

            obj.put("day", reverseConvertDay(result.get(i).day));
            obj.put("time", reverseConvertTime(result.get(i).hour));
            obj.put("nameClass", result.get(i).getName());
            obj.put("Lecturer", result.get(i).getTeacher());
            obj.put("Grupa", result.get(i).getGroup());
            obj.put("Sala", result.get(i).getRoom());
            zapis.println(obj);

        }
        zapis.close();
    }

    /**
     * Funkcja przyjmuję godzinę rozpoczęcia i konwertuję ją do numeru indeksu używanego do poruszania się po tablicy
     * @param timeIndex numer indeksu tablicy godzin
     * @return godzina rozpoczecia zajęc Cytowanie: https://docs.oracle.com/javase/7/docs/api/java/sql/Time.html
     */

    public Time reverseConvertTime(int timeIndex){
        if(timeIndex == 0){
            return new Time(8,0,0);
        }else if(timeIndex == 1){
            return new Time(9,45,0);
        }else if(timeIndex == 2){
            return new Time(11,30,0);
        }else if(timeIndex == 3){
            return new Time(13,15,0);
        }else if(timeIndex == 4){
            return new Time(15,00,0);
        }else if(timeIndex == 5){
            return new Time(16,45,0);
        }else if(timeIndex == 6){
            return new Time(18,30,0);
        }else if(timeIndex == 7){
            return new Time(20,15,0);
        }else{
            return new Time(0,0,0);
        }
    }

    /**
     * Funkcja przyjmuję Stringa z nazwą dnia tygodnia i konwertuję ją do numeru indeksu użwyanego do poruszania się po tablicy
     * @param dayIndex numer indeksu tablicy dni
     * @return nazwa dnia Cytowanie: https://docs.oracle.com/javase/7/docs/api/java/lang/String.html
     */
    public String reverseConvertDay(int dayIndex){
        if(dayIndex == 0){
            return "Poniedziałek";
        }else if(dayIndex == 1){
            return "Wtorek";
        }else if(dayIndex == 2){
            return "Środa";
        }else if(dayIndex == 3){
            return "Czwartek";
        }else if(dayIndex == 4){
            return "Piątek";
        }else {
            return "Weekend";
        }
    }
}