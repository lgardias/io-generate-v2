package generate;

import singletons.DataManager;

/**
 * Klasa Gernerate odpowiada za wygenerowanie planu zajęć
 * na podstawie danych otrzymanych z klasy Input
 * Created by HP on 2017-01-11.
 */
public class Generate {


    /**Obiekt utworzony przez Grupę nr I służący do zainicjowania obiektu klasy Input */
    private DataManager data;

    /**Obiekt z gotowymi tablicami danych */
    public Input DaneWejsciowe;

    /** Tablica 3D reprezentująca plan zajęc */
    public Zajecia [][][] schedule;

    /**Licznik zajęć przypisanych zgodnie z referencjami */
    private int happyRatio = 0;

    /**
     * Metoda inicjalizująca pustą tablicę 3D reprezuntującą plan zajęć
     */
    public void init(){

        schedule = new Zajecia[DaneWejsciowe.groupTab.length][6][8];

        for(int i = 0; i < schedule.length; i++){
            for(int j = 0; j < 6; j++){
                for (int k = 0; k < 8; k++){

                    schedule[i][j][k] = new Zajecia();

                }
            }
        }


    }

    /**
     * Metoda wypsująca plan zajęć dla danego id grupy
     * @param group id grupy któręj ma zostać wypisany plan
     */
    public void showSchedule(int group){
        for(int i = 0; i < 8 ;i++ ){
            for(int j = 0; j < 6; j++){
                System.out.print(schedule[group][j][i].getName()+"|\t\t\t|" + schedule[group][j][i].getRoom()+"|\t\t");

            }
            System.out.println("");
        }
        System.out.println("\n");
        System.out.println("Poziom zgodności z preferencjami: " + (happyRatio/DaneWejsciowe.listClasses.length)*100+"%\n");
    }

    /**
     * Metoda generująca plan zajęc w oparciu o dane klasy Input
     */
    public void generateScheduleSimply() {

        for (int z = 0; z < DaneWejsciowe.listClasses.length; z++) {

            if (DaneWejsciowe.listClasses[z].isFlag()) {

                boolean flag = false;
                int idTeacher = findTeacher(DaneWejsciowe.listClasses[z].getTeacher());
                int idGroup = findGroup(DaneWejsciowe.listClasses[z].getGroup());


                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 8; j++) {

                        if (DaneWejsciowe.teacherTab[idTeacher].availableTab[i][j] == true) {

                            if (DaneWejsciowe.groupTab[idGroup].availableTab[i][j] == false) {

                                if(DaneWejsciowe.listClasses[z].getType().equals("Wykład")){

                                    for(int k = 0; k< DaneWejsciowe.roomTabLecture.length; k++){
                                        if(DaneWejsciowe.roomTabLecture[k].availableTab[i][j] == true){
                                            DaneWejsciowe.listClasses[z].setRoom(DaneWejsciowe.roomTabLecture[k].getId());
                                            DaneWejsciowe.listClasses[z].setFlag(false);
                                            schedule[idGroup][i][j] = DaneWejsciowe.listClasses[z];
                                            DaneWejsciowe.teacherTab[idTeacher].setFlag(i, j, false);
                                            DaneWejsciowe.groupTab[idGroup].setFlag(i, j, true);

                                            happyRatio++;
                                            flag = true;
                                            break;
                                        }
                                    }


                                }else if(DaneWejsciowe.listClasses[z].getType().equals("Zajęcia laboratoryjne")){
                                    for(int k = 0; k< DaneWejsciowe.roomTabLab.length; k++){
                                        if(DaneWejsciowe.roomTabLab[k].availableTab[i][j] == true){
                                            DaneWejsciowe.listClasses[z].setRoom(DaneWejsciowe.roomTabLab[k].getId());
                                            DaneWejsciowe.listClasses[z].setFlag(false);
                                            schedule[idGroup][i][j] = DaneWejsciowe.listClasses[z];
                                            DaneWejsciowe.teacherTab[idTeacher].setFlag(i, j, false);
                                            DaneWejsciowe.groupTab[idGroup].setFlag(i, j, true);
                                            happyRatio++;
                                            flag = true;
                                            break;
                                        }
                                    }

                                }else if(DaneWejsciowe.listClasses[z].getType().equals("Zajęcia audytoryjne") || DaneWejsciowe.listClasses[z].getType().equals("Zajęcia projektowe")){
                                    for(int k = 0; k< DaneWejsciowe.roomTabAudt.length; k++){
                                        if(DaneWejsciowe.roomTabAudt[k].availableTab[i][j] == true){
                                            DaneWejsciowe.listClasses[z].setRoom(DaneWejsciowe.roomTabAudt[k].getId());
                                            DaneWejsciowe.listClasses[z].setFlag(false);
                                            schedule[idGroup][i][j] = DaneWejsciowe.listClasses[z];
                                            DaneWejsciowe.teacherTab[idTeacher].setFlag(i, j, false);
                                            DaneWejsciowe.groupTab[idGroup].setFlag(i, j, true);
                                            happyRatio++;
                                            flag = true;
                                            break;
                                        }
                                    }
                                }


                                       if(flag==true) {
                                           break;
                                       }


                            }

                        }

                        }
                    if (flag) {
                        break;
                    }
                }
            }
        }

    }


    /**
     * Metoda zwracająca indeks grupy o podanej nazwie
     * @param name nazwa grupy ktorej id szukamy
     * @return szukane id
     */
    private int findGroup(String name){
        for (int i = 0; i < DaneWejsciowe.groupTab.length; i++){
            if(name.equals(DaneWejsciowe.groupTab[i].getName()) ){
                return i;
            }
        }
        return -1;
    }

    /**
     * Metoda zwracjąca id prowadzącego danych zajęć
     * @param id id gloalne prowadzącego
     * @return id lokalne w tablicy prowadzących
     */
    private int findTeacher(int id){
        for (int i = 0; i < DaneWejsciowe.teacherTab.length; i++){
            if(id == DaneWejsciowe.teacherTab[i].getId()){
                return i;
            }
        }
        return -1;
    }

    /**
     * Konstruktor umożliwiający stworzenie obiektu, inicjalizuje dane
     * @param dataInput obiekt klasy DataManager stworzony przez programistę Grupy I
     */
    public Generate(DataManager dataInput){

        this.data = dataInput;
        this.DaneWejsciowe = new Input(data);
        init();
}

}
