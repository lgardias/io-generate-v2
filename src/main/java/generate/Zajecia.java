package generate;

/**
 * Klasa Zajęcia służy jako kontener danych oraz obiekt zawierający wszystkie dane o przypisanych zajęciach
 * Created by HP on 2017-01-11.
 */
public class Zajecia {

    /**Flaga określa czy zajęcia zostały przypisane */
    private boolean flag;

    /**Nazwa zajęć */
    private String name;

    /**Nazwa prowadzącego danych zajęć */
    private int teacher;

    /**Id sali w której odbywają się dane zajęcia */
    private int room;

    /**Rodzaj danych zajęć */
    private String type;

    /**Nazwa grupy  */
    private String group;

    /**Indeksy określające dni i godziny w tablicy dosepności */
    public int day, hour;

    /**
     * Konstruktor domyślny do tworzenia pustego obiektu
     */
    public Zajecia(){
        this.name = "";
        this.teacher = 0;
        this.room = 0;
        this.type = "";
        this.flag = true;
        this.group = "";
    }

    /**
     * Konstruktor sparametryzowany służący do tworzenia nie powiązanego z planem obiektu
     * @param name nazwa przedmiotu
     * @param teacher nazwa prowadzącego
     * @param type rodzaj zajęć
     * @param group id grupy
     */
    public Zajecia(String name, int teacher, String type, String group){

        this.name = name;
        this.teacher = teacher;
        this.type = type;
        this.flag = true;
        this.room = 0;
        this.group = group;

    }


    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getTeacher() {
        return teacher;
    }

    public void setTeacher(int teacher) {
        this.teacher = teacher;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
